'use strict';

module.exports = {
  axios: require('./src/axios'),
  fastify: require('./src/fastify'),
  HTTPService: require('./src/HTTPService'),
};
