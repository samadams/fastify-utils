'use strict';

const test = require('ava');
const sinon = require('sinon');
const urljoin = require('url-join');
const ax = require('axios/index');
const MockAdapter = require('axios-mock-adapter');
const proxyquire = require('proxyquire')
  .noCallThru()
  .noPreserveCache();

const { HTTPService } = require('../');

test('HTTPService health (passed axios)', async t => {
  const url = 'http://foo.com/bar';
  const healthUrl = urljoin(url, 'health');
  const axiosInstance = ax.create();
  const moxios = new MockAdapter(axiosInstance);
  moxios.onGet(healthUrl).replyOnce(200, { foo: 'bar' });
  const httpService = new HTTPService({
    url,
    axiosInstance,
  });
  const result = await httpService.health();
  t.is(httpService.url, url);
  t.deepEqual(result.config.url, healthUrl);
  t.deepEqual(result.config.method, 'get');
  t.deepEqual(result.data, { foo: 'bar' });
});

test('HTTPService health (default axios)', async t => {
  const url = 'http://foo.com/bar';
  const healthUrl = urljoin(url, 'health');
  const createAxiosSpy = sinon.stub();
  const axiosInstance = ax.create();
  createAxiosSpy.returns(axiosInstance);
  const HTTPService = proxyquire('../src/HTTPService', {
    axios: {
      create: createAxiosSpy,
    },
  });
  const moxios = new MockAdapter(axiosInstance);
  moxios.onGet(healthUrl).replyOnce(200, { foo: 'bar' });
  // monkeypatch axios create method
  const httpService = new HTTPService({
    url,
    // axiosInstance, NOT PASSED
  });
  const result = await httpService.health();
  t.is(httpService.url, url);
  t.deepEqual(createAxiosSpy.args, [
    [
      {
        timeout: 2000,
      },
    ],
  ]);
  t.deepEqual(result.config.url, healthUrl);
  t.deepEqual(result.config.method, 'get');
  t.deepEqual(result.data, { foo: 'bar' });
});
