'use strict';

const test = require('ava');

const { fastify } = require('../');

const { getLoggerDefaults } = fastify;

test('fastify - getLoggerDefaults - correct default timestamp', async t => {
  const loggerOpts = getLoggerDefaults({ other: 'foo' });
  t.is(loggerOpts.other, 'foo');
  t.regex(loggerOpts.timestamp(), /^,"time":\d{10}$/);
  const stamp = loggerOpts.timestamp().split(':')[1];
  t.is(Date.now() / 1000 - stamp < 1, true, 'log entry time is similar to current time');
});

test('fastify - getLoggerDefaults - changed timestamp', async t => {
  const loggerOpts = getLoggerDefaults({ timestamp: () => ',"time":12345' });
  const stamp = loggerOpts.timestamp();
  t.is(stamp, ',"time":12345');
});
