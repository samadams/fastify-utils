'use strict';

const test = require('ava');
const { BadGateway } = require('http-errors');
const { create: createAxios } = require('axios');
const MockAdapter = require('axios-mock-adapter');

const { axios } = require('../');

[
  {
    title: 'network error',
    setup: maxios => maxios.onAny().networkError(),
    message: 'http://myurl/foo/bar (get): Network Error - No response received',
  },
  {
    title: 'timeout',
    setup: maxios => maxios.onAny().timeout(),
    message: 'http://myurl/foo/bar (get): timeout of 0ms exceeded - No response received',
  },
  {
    title: '500',
    setup: maxios => maxios.onAny().replyOnce(500, { status: 500, arbdata: 'FOOO' }),
    message:
      'http://myurl/foo/bar (get): Request failed with status code 500 - Response payload: "{"status":500,"arbdata":"FOOO"}"',
  },
].forEach(({ title, setup, message }) => {
  test(`axios - ${title}`, async t => {
    const axiosInstance = createAxios();
    const mockAxios = new MockAdapter(axiosInstance);
    const expectPassedToAxios = { url: 'http://myurl/foo/bar' };
    setup(mockAxios);
    await t.throwsAsync(
      async () => {
        await axios(axiosInstance)(expectPassedToAxios);
        t.is(mockAxios.args[0], expectPassedToAxios);
      },
      {
        instanceOf: BadGateway,
        message,
      },
    );
  });
});
