'use strict';

const test = require('ava');
const sinon = require('sinon');
const proxyquire = require('proxyquire')
  .noCallThru()
  .noPreserveCache();
const { fastify } = require('../');

const {
  createErrorHandler,
  serviceHealth,
  mongoHealth,
  healthResponse,
  handleFatalError,
  setSchemaTypeDefaults,
} = fastify;

test('fastify - handleFatalError - closes and exits', async t => {
  const nextTick = process.nextTick;
  const err = new Error('foo');
  const nextTickStub = sinon.stub();
  const fastifyCloseSpy = sinon.stub();
  const fastifyLogSpy = sinon.stub();
  const fakeFastify = { close: fastifyCloseSpy, log: { fatal: fastifyLogSpy } };
  process.nextTick = nextTickStub;
  fastifyCloseSpy.resolves();
  await t.throwsAsync(
    async () => {
      handleFatalError(fakeFastify, err);
      await nextTickStub.args[0][0]();
    },
    {
      message: 'foo',
    },
  );
  t.is(fastifyCloseSpy.args.length, 1);
  t.is(fastifyLogSpy.args.length, 1);
  t.is(nextTickStub.args.length, 1);
  t.is(fastifyLogSpy.args[0][0].message, 'foo');
  process.nextTick = nextTick;
});

[
  {
    statusCode: 500,
    errorMessage: 'foo',
    expected: {
      errLog: 'foo',
      handlerErrorMessage: 'foo',
      publicMessage: 'Internal Server Error',
    },
  },
  {
    statusCode: 502,
    errorMessage: 'foo',
    expected: {
      errLog: 'foo',
      handlerErrorMessage: null,
      publicMessage: 'Bad Gateway',
    },
  },
  {
    statusCode: 400,
    errorMessage: 'foo',
    expected: {
      errLog: null,
      handlerErrorMessage: null,
      publicMessage: 'foo',
    },
  },
].forEach(({ statusCode, errorMessage, expected }) => {
  test(`fastify - errorHookHandler - ${statusCode}`, async t => {
    const errLogSpy = sinon.spy();
    const fatalErrorSpy = sinon.spy();
    const fastify = { log: { error: errLogSpy } };
    const handler = createErrorHandler(fastify, fatalErrorSpy);
    await t.throwsAsync(
      async () => {
        await handler(new Error(errorMessage), null, { res: { statusCode } });
      },
      {
        message: expected.publicMessage,
      },
    );

    if (expected.errLog) {
      t.is(errLogSpy.args.length, 1);
      const [errorLogMessage] = errLogSpy.args[0];
      t.is(errorLogMessage.message, expected.errLog);
    } else {
      t.is(errLogSpy.args.length, 0);
    }

    if (expected.handlerErrorMessage) {
      t.is(fatalErrorSpy.args.length, 1);
      const [fatalHandlerFastify, fatalHandlerError] = fatalErrorSpy.args[0];
      t.is(fatalHandlerFastify, fastify);
      t.is(fatalHandlerError.message, expected.handlerErrorMessage);
    } else {
      t.is(fatalErrorSpy.args.length, 0);
    }
  });
});

[
  {
    title: '200',
    res: () => Promise.resolve({ status: 200, data: 'na' }),
    expected: {
      ok: true,
    },
  },
  {
    title: '201',
    res: () => Promise.resolve({ status: 201, data: '201 data' }),
    expected: {
      ok: false,
      details: 'Unexpected status 201 from "something" service - body: "201 data"',
    },
  },
  {
    title: 'unexpected error',
    res: () => Promise.reject(new Error('something bad')),
    expected: {
      ok: false,
      details: '"something" service error: Error: something bad',
    },
  },
].forEach(({ title, res, expected }) => {
  test(`fastify - serviceHealth - ${title}`, async t => {
    const actual = await serviceHealth('something', {
      health: res,
    });
    t.deepEqual(actual, expected);
  });
});

test('fastify - mongoHealth - ok', async t => {
  const actual = await mongoHealth({
    db: {
      stats: async () => ({ ok: true }),
    },
  });
  t.deepEqual(actual, { ok: true });
});

test('fastify - mongoHealth - not ok', async t => {
  const actual = await mongoHealth({
    db: {
      stats: async () => ({ ok: false, other: 'blah' }),
    },
  });
  t.deepEqual(actual, {
    ok: false,
    details: 'Mongo stats report !ok: {"ok":false,"other":"blah"}',
  });
});

test('fastify - mongoHealth - not ok (unexpected)', async t => {
  const actual = await mongoHealth({
    db: {
      stats: async () => {
        throw new Error(':(');
      },
    },
  });
  t.deepEqual(actual, {
    ok: false,
    details: 'Unknown mongo error: Error: :(',
  });
});

[
  {
    title: 'no errors',
    dependencies: {
      foo: { ok: true },
      bar: { ok: true },
    },
    expected: {
      errLog: null,
      status: 200,
      body: {
        dependencies: {
          bar: true,
          foo: true,
        },
        status: 'OK',
        statusCode: 200,
      },
    },
  },
  {
    title: 'two deps, mixed status',
    dependencies: {
      foo: { ok: true, details: 'deets true' },
      bar: { ok: false, details: 'deets bar' },
    },
    expected: {
      errLog: 'Health check for bar failed: deets bar',
      status: 200,
      body: {
        dependencies: {
          bar: false,
          foo: true,
        },
        status: 'UNHEALTHY',
        statusCode: 200,
      },
    },
  },
].forEach(({ title, dependencies, expected }) => {
  test(`fastify - healthResponse - ${title}`, async t => {
    const errLogSpy = sinon.spy();
    const replySendSpy = sinon.stub();
    const replyCodeSpy = sinon.stub();
    replyCodeSpy.returns({ send: replySendSpy });
    const reply = { code: replyCodeSpy };
    const fastify = { log: { error: errLogSpy } };
    healthResponse(dependencies, fastify, reply);

    if (expected.errLog) {
      t.is(errLogSpy.args.length, 1);
      const [errorLogMessage] = errLogSpy.args[0];
      t.is(errorLogMessage, expected.errLog);
    } else {
      t.is(errLogSpy.args.length, 0);
    }

    t.is(replySendSpy.args.length, 1);
    t.is(replyCodeSpy.args.length, 1);

    const [actualCode] = replyCodeSpy.args[0];
    const [actualBody] = replySendSpy.args[0];
    t.is(actualCode, expected.status);
    t.deepEqual(actualBody, expected.body);
  });
});

[
  {
    title: 'default one type, no nesting',
    schema: {
      type: 'object',
      properties: {
        email: { type: 'string', format: 'email' },
      },
    },
    defaults: {
      object: {
        foo: 'bar',
      },
    },
    expected: {
      type: 'object',
      foo: 'bar',
      properties: {
        email: { type: 'string', format: 'email' },
      },
    },
  },
  {
    title: 'default multiple types, with nesting',
    schema: {
      type: 'object',
      properties: {
        oJect: {
          type: 'object',
          properties: {
            subOjectOject: {
              type: 'string',
              something: 'stringy',
            },
          },
        },
        arrRayOject: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              subArrRayOject: {
                type: 'string',
                something: 'stringy',
              },
              subArrRayArrRay: {
                type: 'array',
                items: [{ type: 'string' }, { type: 'object' }],
              },
            },
          },
        },
        arrArr: {
          type: 'array',
          items: [{ type: 'string' }, { type: 'object' }],
        },
        dodgyArray: {
          type: 'array',
          items: 'dodgy',
        },
      },
    },
    defaults: {
      object: {
        objectExtra: 'slobject',
      },
      string: {
        stringExtra: 'stringo',
      },
      array: {
        arrayExtra: 'barray',
      },
    },
    expected: {
      type: 'object',
      properties: {
        oJect: {
          type: 'object',
          properties: {
            subOjectOject: { type: 'string', something: 'stringy', stringExtra: 'stringo' },
          },
          objectExtra: 'slobject',
        },
        arrRayOject: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              subArrRayOject: { type: 'string', something: 'stringy', stringExtra: 'stringo' },
              subArrRayArrRay: {
                type: 'array',
                items: [
                  { type: 'string', stringExtra: 'stringo' },
                  { type: 'object', objectExtra: 'slobject' },
                ],
                arrayExtra: 'barray',
              },
            },
            objectExtra: 'slobject',
          },
          arrayExtra: 'barray',
        },
        arrArr: {
          type: 'array',
          items: [
            { type: 'string', stringExtra: 'stringo' },
            { type: 'object', objectExtra: 'slobject' },
          ],
          arrayExtra: 'barray',
        },
        dodgyArray: {
          type: 'array',
          items: 'dodgy',
          arrayExtra: 'barray',
        },
      },
      objectExtra: 'slobject',
    },
  },
].forEach(({ title, schema, defaults, expected }) => {
  test(`fastify - setSchemaDefaults - ${title}`, async t => {
    const returnedSchema = setSchemaTypeDefaults(schema, defaults);
    t.deepEqual(returnedSchema, expected);
  });
});

test('fastify - strictSchemaCompiler - correct defaults', async t => {
  const ajvMock = sinon.stub();
  const ajvCompileMock = sinon.stub();
  const expectedAjvInstance = {};
  ajvCompileMock.returns(expectedAjvInstance);
  ajvMock.returns({
    compile: ajvCompileMock,
  });
  const { strictSchemaCompiler } = proxyquire('../src/fastify', {
    ajv: ajvMock,
  });

  const ajvInstance = strictSchemaCompiler({
    type: 'object',
    properties: {
      email: { type: 'string', format: 'email' },
      oj: { type: 'object', properties: { type: 'string' } },
    },
  });
  t.is(ajvInstance, expectedAjvInstance);
  t.deepEqual(ajvMock.args, [
    [
      {
        allErrors: true,
        coerceTypes: true,
        removeAdditional: true,
        useDefaults: true,
      },
    ],
  ]);
  t.deepEqual(ajvCompileMock.args, [
    [
      {
        additionalProperties: false,
        properties: {
          email: {
            format: 'email',
            type: 'string',
          },
          oj: {
            additionalProperties: false,
            properties: {
              type: 'string',
            },
            type: 'object',
          },
        },
        type: 'object',
      },
    ],
  ]);
});
