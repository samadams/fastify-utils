# fastify utils

Utilities for writing fastify services.

# install 

`npm install https://gitlab.com/ctrlio/fastify-utils`

# dev

```npm run dev:test```

## release

```npm version patch```

## reformat code

```npm run reformat```

# usage

## axios

Not really fastify related:

Format axios error response as [BadGateway](https://www.npmjs.com/package/http-errors) message:
 
```javascript 1.8
const {axios} = require('fastify-utils');
axios({url: 'http://foo'}).catch(console.error);
// http://foo (get): Request failed with status code 500 - Response payload: "{"status":500,"arbdata":"FOOO"}",
```

## fastify

### error handling

For handling request errors

```javascript 1.8
const {
  createErrorHandler,
  handleFatalError,
} = require('fastify-utils').fastify;

fastify.setErrorHandler(
  // `createErrorHandler` will make an async function which you can set as fastify's error handler.
  // The handler checks the statusCode of the response, and if 500+, will change the body to remove 
  // the error message (for security), log an error with the original error message (for sanity), 
  // and call the fatalErrorHandler (if statusCode < 500, nothing is changed). 
  createErrorHandler(
    fastify, 
    // `handleFatalError` will log a fatal error, wait until the nextTick to close fastify (to give 
    // it a chance to finish the request) and then re-throw (which should kill the node process).
    handleFatalError
  )
)
```

### health check

For formatting health check and dependencies.

```javascript 1.8
const {HttpService} = require('fastify-utils');
const {
  serviceHealth,
  mongoHealth,
  healthResponse,
} = require('fastify-utils').fastify;
const serviceInstance = new HttpService({
  url: 'http://someurl', 
  axiosInstance: undefined,
});
fastify.get('/health', async function(req, reply) {
  return healthResponse(
    {
      mongo: await mongoHealth(fastify.mongo),
      someApi: await serviceHealth('someservice', serviceInstance),
    },
    fastify,
    reply,
  );
});

```

both `mongoHealth` and `serviceHealth` will return a structure like: 

```
{ok: false, details: 'some reason'};
```

and `healthResponse` will collate them into a single response:
```
{
  statusCode: 200,
  status: 'UNHEALTHY',
  dependencies: {'someApi': false, 'mongo': true},
}
 ```

It's always a 200 response because the service is UP, even though it's dependencies are DOWN 
(and we don't want a cascade failure).

Dependency failure details will be logged at error level.

### schema parser

helpers for applying defaults to all AJV schemas:

```javascript 1.8
const {
  strictSchemaCompiler,
  setSchemaTypeDefaults,
} = require('fastify-utils').fastify;

// `strictSchemaCompiler` simply recursively adds `additionalProperties: false` for object type
// schemas (where not already set).
// This means you will get errors if users hit the api with any extraneous properties 
// which prevents you from having to remember to add it to every level of your schema definitions.
fastify.setSchemaCompiler(strictSchemaCompiler);

// `setSchemaTypeDefaults` gives you more control over for which types the default apply.
// you pass the schema and a map of type->defaults to be applied recursively:
fastify.setSchemaCompiler(function(schema){
  // recursively default the schema so additionalProperties always causes an error
  return new Ajv(ajvConfig).compile(
    setSchemaTypeDefaults(schema, { 
      object: { additionalProperties: false },
      array: { uniqueItems: true } 
    })
  );
});
```

### logger
```javascript 1.8
const {
  logger
} = require('fastify-utils').fastify;
const fastify = require('fastify')({ logger })
```
