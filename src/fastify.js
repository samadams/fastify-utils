'use strict';

const createError = require('http-errors');
const Ajv = require('ajv');

module.exports.createErrorHandler = (fastify, fatalErrorHandler) => {
  return async (error, req, { res }) => {
    if (res.statusCode >= 500) {
      // we don't want to share the message, so create generic error from code instead
      const origError = error;
      fastify.log.error(origError);
      error = createError(res.statusCode);
      if (res.statusCode === 500) {
        // 500 means unhandleable, so die:
        fatalErrorHandler(fastify, origError);
      }
    }
    throw error;
  };
};

module.exports.handleFatalError = (fastify, err) => {
  fastify.log.fatal(err);
  process.nextTick(async () => {
    // nextTick gives fastify a chance to close the current response.
    await fastify.close();
    // throw an error in nextTick means fastify can't handle it crashes the server (desired).
    throw err;
  });
};

module.exports.serviceHealth = async (name, service) => {
  try {
    const { status, data } = await service.health();
    if (status === 200) {
      return { ok: true };
    }
    return {
      ok: false,
      details: `Unexpected status ${status} from "${name}" service - body: ${JSON.stringify(data)}`,
    };
  } catch (error) {
    return {
      ok: false,
      details: `"${name}" service error: ${error}`,
    };
  }
};

module.exports.mongoHealth = async mongo => {
  let details;
  try {
    const stats = await mongo.db.stats();
    if (stats.ok) {
      return { ok: true };
    }
    details = `Mongo stats report !ok: ${JSON.stringify(stats)}`;
  } catch (error) {
    details = `Unknown mongo error: ${error.toString()}`;
  }
  return {
    ok: false,
    details,
  };
};

module.exports.healthResponse = (dependencies, fastify, reply) => {
  // since this service is managing to respond, we always return 200 (prevent cascade failures)
  const overallCode = 200;
  let hasErrors = false;
  const outputDeps = {};
  for (const service of Object.keys(dependencies)) {
    const { ok, details } = dependencies[service];
    if (!ok) {
      hasErrors = true;
      fastify.log.error(`Health check for ${service} failed: ${details}`);
    }
    outputDeps[service] = ok;
  }
  return reply.code(overallCode).send({
    statusCode: overallCode,
    status: hasErrors ? 'UNHEALTHY' : 'OK',
    dependencies: outputDeps,
  });
};

const setSchemaTypeDefaults = (schema, defaults) => {
  const { type, ...rest } = schema;
  if (!type) {
    return schema;
  }

  switch (type) {
    case 'object':
      if (typeof rest.properties === 'object') {
        for (const prop of Object.keys(rest.properties)) {
          rest.properties[prop] = setSchemaTypeDefaults(rest.properties[prop], defaults);
        }
      }
      break;
    case 'array':
      if (Array.isArray(rest.items)) {
        rest.items = rest.items.map(item => setSchemaTypeDefaults(item, defaults));
      } else if (typeof rest.items === 'object') {
        rest.items = setSchemaTypeDefaults(rest.items, defaults);
      }
      break;
  }
  const typeDefaults = defaults[type] || {};
  return { type, ...rest, ...typeDefaults };
};

module.exports.setSchemaTypeDefaults = setSchemaTypeDefaults;

module.exports.strictSchemaCompiler = schema => {
  // recursively default the schema so additionalProperties always causes an error
  return new Ajv({
    // keep their default: https://github.com/fastify/fastify/blob/master/docs/Validation-and-Serialization.md#user-content-schema-compiler
    removeAdditional: true,
    useDefaults: true,
    coerceTypes: true,
    allErrors: true,
  }).compile(setSchemaTypeDefaults(schema, { object: { additionalProperties: false } }));
};

module.exports.getLoggerDefaults = opts => {
  opts['timestamp'] = opts['timestamp'] || (() => `,"time":${Math.floor(Date.now() / 1000.0)}`);
  return opts;
};
