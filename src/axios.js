'use strict';

const { BadGateway } = require('http-errors');

module.exports = axiosInstance => {
  return async req => {
    try {
      return await axiosInstance(req);
    } catch (error) {
      let message = error.message;
      message = `${error.config.url} (${error.config.method}): ${message}`;
      if (error.response) {
        // The request was made and the server responded with a status code that fails `validateStatus`
        message = `${message} - Response payload: "${JSON.stringify(error.response.data)}"`;
      } else {
        message = `${message} - No response received`;
      }
      throw new BadGateway(message);
    }
  };
};
