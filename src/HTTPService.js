'use strict';

const { create: createAxios } = require('axios');
const urljoin = require('url-join');

const ctrlioAxios = require('./axios');

module.exports = class HttpService {
  constructor({ url, axiosInstance = createAxios({ timeout: 2000 }) }) {
    this.url = url;
    this.axios = ctrlioAxios(axiosInstance);
  }

  async health() {
    return this.axios({
      method: 'get',
      url: urljoin(this.url, '/health'),
    });
  }
};
