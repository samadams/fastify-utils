module.exports = {
  "plugins": [
    "prettier",
  ],
  "env": {
    "node": true,
    "es6": true,
  },
  "extends": "eslint:recommended",
  "rules": {
    "prettier/prettier": "error",
    strict: ["error", "global"],
    "no-unused-vars": ['error', { "ignoreRestSiblings": true, "argsIgnorePattern": "^_" }],
    "no-console": ["error", { allow: ["warn", "error", "info"] }],
    "prefer-arrow-callback": "error",
    "no-async-promise-executor": "error",
    "no-return-await": "error",
    quotes: [
      "error",
      "single",
      { avoidEscape: true, allowTemplateLiterals: false }
    ]
  },
  parserOptions: {
    ecmaVersion: 2018, // Allows for the parsing of modern ECMAScript features
  },
};
